/* SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2023-2024 Soren Stoutner <soren@stoutner.com>
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// Application headers.
#include "EditBookmarkDialog.h"
#include "ui_EditBookmarkDialog.h"
#include "databases/BookmarksDatabase.h"

// Qt toolkit headers.
#include <QFileDialog>

// Construct the class.
EditBookmarkDialog::EditBookmarkDialog(QWidget *parentWidgetPointer, const int databaseId, QIcon &currentWebsiteFavoriteIcon) : QDialog(parentWidgetPointer), bookmarkDatabaseId(databaseId)
{
    // Set the window title.
    setWindowTitle(i18nc("The edit bookmark dialog window title.", "Edit Bookmark"));

    // Set the window modality.
    setWindowModality(Qt::WindowModality::ApplicationModal);

    // Instantiate the edit bookmark dialog UI.
    Ui::EditBookmarkDialog editBookmarkDialogUi;

    // Setup the UI.
    editBookmarkDialogUi.setupUi(this);

    // Get handles for the widgets.
    currentFavoriteIconRadioButtonPointer = editBookmarkDialogUi.currentFavoriteIconRadioButton;
    currentWebsiteFavoriteIconRadioButtonPointer = editBookmarkDialogUi.currentWebsiteFavoriteIconRadioButton;
    customFavoriteIconRadioButtonPointer = editBookmarkDialogUi.customFavoriteIconRadioButton;
    parentFolderTreeWidgetPointer = editBookmarkDialogUi.parentFolderTreeWidget;
    bookmarkNameLineEditPointer = editBookmarkDialogUi.bookmarkNameLineEdit;
    bookmarkUrlLineEditPointer = editBookmarkDialogUi.bookmarkUrlLineEdit;
    QPushButton *browseButtonPointer = editBookmarkDialogUi.browseButton;
    QDialogButtonBox *dialogButtonBoxPointer = editBookmarkDialogUi.dialogButtonBox;
    saveButtonPointer = dialogButtonBoxPointer->button(QDialogButtonBox::Save);

    // Get the bookmark struct.
    bookmarkStructPointer = BookmarksDatabase::getBookmark(databaseId);

    // Set the favorite icons.
    currentFavoriteIconRadioButtonPointer->setIcon(bookmarkStructPointer->favoriteIcon);
    currentWebsiteFavoriteIconRadioButtonPointer->setIcon(currentWebsiteFavoriteIcon);
    customFavoriteIconRadioButtonPointer->setIcon(QIcon::fromTheme(QLatin1String("globe"), QIcon::fromTheme(QLatin1String("applications-internet"))));

    // Instantiate a folder helper.
    folderHelperPointer = new FolderHelper();

    // Set the parent folder tree widget column count.
    parentFolderTreeWidgetPointer->setColumnCount(2);

    // Hide the second column.
    parentFolderTreeWidgetPointer->hideColumn(folderHelperPointer->FOLDER_ID_COLUMN);

    // Set the column header.
    parentFolderTreeWidgetPointer->setHeaderLabel(i18nc("The folder tree widget header", "Select Parent Folder"));

    // Create a bookmarks tree widget item.
    QTreeWidgetItem *bookmarksTreeWidgetItemPointer = new QTreeWidgetItem();

    // Populate the bookmarks tree widget item.
    bookmarksTreeWidgetItemPointer->setText(folderHelperPointer->FOLDER_NAME_COLUMN, i18nc("The bookmarks root tree widget name", "Bookmarks"));
    bookmarksTreeWidgetItemPointer->setIcon(folderHelperPointer->FOLDER_NAME_COLUMN, QIcon::fromTheme(QLatin1String("bookmarks"), QIcon::fromTheme(QLatin1String("bookmark-new"))));
    bookmarksTreeWidgetItemPointer->setText(folderHelperPointer->FOLDER_ID_COLUMN, QLatin1String("0"));

    // Add the bookmarks tree widget item to the root of the tree.
    parentFolderTreeWidgetPointer->addTopLevelItem(bookmarksTreeWidgetItemPointer);

    // Select the root bookmarks folder if it is the initial parent folder.
    if (bookmarkStructPointer->parentFolderId == 0)
        bookmarksTreeWidgetItemPointer->setSelected(true);

    // Populate the subfolders.
    folderHelperPointer->populateSubfolders(bookmarksTreeWidgetItemPointer, bookmarkStructPointer->parentFolderId);

    // Open all the folders.
    parentFolderTreeWidgetPointer->expandAll();

    // Populate the line edits.
    bookmarkNameLineEditPointer->setText(bookmarkStructPointer->name);
    bookmarkUrlLineEditPointer->setText(bookmarkStructPointer->url);

    // Scroll to the beginning of the bookmark URL line edit.
    bookmarkUrlLineEditPointer->setCursorPosition(0);

    // Focus the bookmark name line edit.
    bookmarkNameLineEditPointer->setFocus();

    // Connect the buttons.
    connect(browseButtonPointer, SIGNAL(clicked()), this, SLOT(browse()));
    connect(dialogButtonBoxPointer, SIGNAL(accepted()), this, SLOT(save()));
    connect(dialogButtonBoxPointer, SIGNAL(rejected()), this, SLOT(reject()));

    // Update the UI when the line edits change.
    connect(bookmarkNameLineEditPointer, SIGNAL(textEdited(const QString&)), this, SLOT(updateUi()));
    connect(bookmarkUrlLineEditPointer, SIGNAL(textEdited(const QString&)), this, SLOT(updateUi()));

    // Set the initial UI status.
    updateUi();
}

void EditBookmarkDialog::browse()
{
    // Get an image file string from the user.
    QString imageFileString = QFileDialog::getOpenFileName(this, i18nc("The browse for favorite icon dialog header", "Favorite Icon Image"), QDir::homePath(),
                              i18nc("The browse for image files filter", "Image Files — *.bmp, *.gif, *.jpg, *.jpeg, *.png, *.svg(*.bmp *.gif *.jpg *.jpeg *.png *.svg);;All Files(*)"));


    // Check to see if an image file string was returned.  This will be empty if the user selected cancel.
    if (!imageFileString.isEmpty())
    {
        // Set the custom favorite icon.
        customFavoriteIconRadioButtonPointer->setIcon(QIcon(imageFileString));

        // Check the custom favorite icon radio button.
        customFavoriteIconRadioButtonPointer->setChecked(true);
    }
}

void EditBookmarkDialog::save()
{
    // Get the selected folders list.
    QList<QTreeWidgetItem*> selectedFoldersList = parentFolderTreeWidgetPointer->selectedItems();

    // Get the selected folder.
    QTreeWidgetItem *selectedFolderPointer = selectedFoldersList.first();

    // Get the parent folder ID.
    double parentFolderId = selectedFolderPointer->text(folderHelperPointer->FOLDER_ID_COLUMN).toDouble();

    // Get the original display order.
    int displayOrder = bookmarkStructPointer->displayOrder;

    // Get the new display order if the parent folder has changed.
    if (parentFolderId != bookmarkStructPointer->parentFolderId)
        displayOrder = BookmarksDatabase::getFolderItemCount(parentFolderId);

    // Create a favorite icon.
    QIcon favoriteIcon;

    // Get the favorite icon.
    if (currentFavoriteIconRadioButtonPointer->isChecked())  // The current favorite icon is checked.
        favoriteIcon = currentFavoriteIconRadioButtonPointer->icon();
    else if (currentWebsiteFavoriteIconRadioButtonPointer->isChecked())  // The current website favorite icon is checked.
        favoriteIcon = currentWebsiteFavoriteIconRadioButtonPointer->icon();
    else  // The custom favorite icon is checked.
        favoriteIcon = customFavoriteIconRadioButtonPointer->icon();

    // Create a bookmark struct.
    BookmarkStruct *updatedBookmarkStructPointer = new BookmarkStruct;

    // Populate the bookmark struct.
    updatedBookmarkStructPointer->databaseId = bookmarkDatabaseId;
    updatedBookmarkStructPointer->name = bookmarkNameLineEditPointer->text();
    updatedBookmarkStructPointer->url = bookmarkUrlLineEditPointer->text();
    updatedBookmarkStructPointer->parentFolderId = parentFolderId;
    updatedBookmarkStructPointer->displayOrder = displayOrder;
    updatedBookmarkStructPointer->favoriteIcon = favoriteIcon;

    // Update the bookmark.
    BookmarksDatabase::updateBookmark(updatedBookmarkStructPointer);

    // Update the display order of all the items in the previous folder.
    BookmarksDatabase::updateFolderContentsDisplayOrder(bookmarkStructPointer->parentFolderId);

    // Emit the bookmark saved signal.
    Q_EMIT bookmarkSaved();

    // Close the dialog.
    close();
}

void EditBookmarkDialog::updateUi()
{
    // Determine if both line edits are populated.
    if (bookmarkNameLineEditPointer->text().isEmpty() || bookmarkUrlLineEditPointer->text().isEmpty())  // At least one of the line edits is empty.
    {
        // Disable the save button.
        saveButtonPointer->setEnabled(false);
    }
    else  // Both of the line edits are populated.
    {
        // Enable the save button.
        saveButtonPointer->setEnabled(true);
    }
}
