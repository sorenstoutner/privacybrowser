/* SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022, 2024 Soren Stoutner <soren@stoutner.com>
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DOMAIN_SETTINGS_DIALOG_H
#define DOMAIN_SETTINGS_DIALOG_H

// Application headers.
#include "helpers/UserAgentHelper.h"

// KDE Frameworks headers.
#include <KLineEdit>

// Qt toolkit headers.
#include <QDialog>
#include <QDoubleSpinBox>
#include <QLabel>
#include <QtSql>

class DomainSettingsDialog : public QDialog
{
    // Include the Q_OBJECT macro.
    Q_OBJECT

public:
    // The primary constructor.
    explicit DomainSettingsDialog(QWidget *parentWidgetPointer, const int &startType = SHOW_ALL_DOMAINS, const QString &domainName = QStringLiteral(""));

    // The public static int constants.
    static const int SHOW_ALL_DOMAINS;
    static const int ADD_DOMAIN;
    static const int EDIT_DOMAIN;

Q_SIGNALS:
    void domainSettingsUpdated() const;

private Q_SLOTS:
    // The private slots.
    void apply() const;
    void cancel();
    void customZoomFactorChanged(const double newValue) const;
    void domStorageChanged(const int newIndex) const;
    void domainNameChanged(const QString &updatedDomainName) const;
    void domainSelected(const QModelIndex &modelIndex) const;
    void easyListChanged(const int newIndex) const;
    void easyPrivacyChanged(const int newIndex) const;
    void fanboysAnnoyanceListChanged(const int newIndex) const;
    void javaScriptChanged(const int newIndex) const;
    void localStorageChanged(const int newIndex) const;
    void ok();
    void reset() const;
    void showAddMessageBox();
    void showDeleteAllMessageBox() const;
    void showDeleteMessageBox() const;
    void ultraListChanged(const int newIndex) const;
    void ultraPrivacyChanged(const int newIndex) const;
    void userAgentChanged(const QString &updatedUserAgent) const;
    void zoomFactorComboBoxChanged(const int newIndex) const;

private:
    // The private constants.
    static const int JAVASCRIPT = 0;
    static const int LOCAL_STORAGE = 1;
    static const int DOM_STORAGE = 2;
    static const int ULTRAPRIVACY = 3;
    static const int ULTRALIST = 4;
    static const int EASYPRIVACY = 5;
    static const int EASYLIST = 6;
    static const int FANBOYS_ANNOYANCE_LIST = 7;

    // The private variables.
    QPushButton *applyButtonPointer;
    QDoubleSpinBox *customZoomFactorSpinBoxPointer;
    QPalette defaultPalette;
    QPushButton *deleteAllDomainsButtonPointer;
    QPushButton *deleteDomainButtonPointer;
    QWidget *domStorageWidgetPointer;
    QComboBox *domStorageComboBoxPointer;
    QLabel *domStorageLabelPointer;
    KLineEdit *domainNameLineEditPointer;
    QWidget *domainSettingsWidgetPointer;
    QListView *domainsListViewPointer;
    QSqlTableModel *domainsTableModelPointer;
    QItemSelectionModel *domainsSelectionModelPointer;
    QWidget *easyListWidgetPointer;
    QComboBox *easyListComboBoxPointer;
    QLabel *easyListLabelPointer;
    QWidget *easyPrivacyWidgetPointer;
    QComboBox *easyPrivacyComboBoxPointer;
    QLabel *easyPrivacyLabelPointer;
    QWidget *fanboysAnnoyanceListWidgetPointer;
    QComboBox *fanboysAnnoyanceListComboBoxPointer;
    QLabel *fanboysAnnoyanceListLabelPointer;
    QPalette highlightedPalette;
    QWidget *javaScriptWidgetPointer;
    QComboBox *javaScriptComboBoxPointer;
    QLabel *javaScriptLabelPointer;
    QWidget *localStorageWidgetPointer;
    QComboBox *localStorageComboBoxPointer;
    QLabel *localStorageLabelPointer;
    QPushButton *resetButtonPointer;
    QWidget *ultraListWidgetPointer;
    QComboBox *ultraListComboBoxPointer;
    QLabel *ultraListLabelPointer;
    QWidget *ultraPrivacyWidgetPointer;
    QComboBox *ultraPrivacyComboBoxPointer;
    QLabel *ultraPrivacyLabelPointer;
    QWidget *userAgentWidgetPointer;
    QComboBox *userAgentComboBoxPointer;
    UserAgentHelper *userAgentHelperPointer;
    QLabel *userAgentLabelPointer;
    QWidget *zoomFactorWidgetPointer;
    QComboBox *zoomFactorComboBoxPointer;

    // The private functions.
    void populateLabel(const int label) const;
    void populateUserAgentLabel(const QString &userAgentName) const;
    void updateButtons() const;
    void updateDomStorageStatus() const;
};
#endif
