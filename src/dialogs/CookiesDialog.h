/* SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022-2024 Soren Stoutner <soren@stoutner.com>
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef COOKIESDIALOG_H
#define COOKIESDIALOG_H

// Qt toolkit headers.
#include <QDialog>
#include <QItemSelectionModel>
#include <QNetworkCookie>
#include <QStandardItemModel>
#include <QTreeView>

// C++ headers.
#include <list>

class CookiesDialog : public QDialog
{
    // Include the Q_OBJECT macro.
    Q_OBJECT

public:
    // The primary constructor.
    explicit CookiesDialog(std::list<QNetworkCookie> *cookieListPointer);

Q_SIGNALS:
    // The signals.
    void addCookie(const QNetworkCookie &cookie) const;
    void deleteAllCookies() const;
    void deleteCookie(const QNetworkCookie &cookie) const;

private Q_SLOTS:
    // The private slots.
    void addCookieFromDialog(const QNetworkCookie &cookie, const bool &isDurable) const;
    void deleteCookieFromDatabase(const QNetworkCookie &cookie) const;
    void deleteCookieFromDialog(const QNetworkCookie &cookie) const;
    void showAddCookieDialog();
    void showDeleteAllMessageBox() const;
    void showDeleteCookieMessageBox() const;
    void showDurableCookiesDialog();
    void showEditCookieDialog();
    void updateUi() const;

private:
    // The private variables.
    QPushButton *addCookieButtonPointer;
    std::list<QNetworkCookie> *cookieListPointer;
    QPushButton *deleteAllButtonPointer;
    QPushButton *deleteCookieButtonPointer;
    QPushButton *durableCookiesButtonPointer;
    QPushButton *editCookieButtonPointer;
    QStandardItemModel *treeModelPointer;
    QItemSelectionModel *treeSelectionModelPointer;
    QTreeView *treeViewPointer;

    // The private functions.
    void deleteCookie(const QModelIndex &modelIndex, const bool &deleteDurableCookies) const;
    void deleteDomain(const QModelIndex &modelIndex, const bool &deleteDurableCookies) const;
};
#endif
