/* SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022-2025 Soren Stoutner <soren@stoutner.com>
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef URL_REQUEST_INTERCEPTOR_H
#define URL_REQUEST_INTERCEPTOR_H

// Application headers.
#include "structs/RequestStruct.h"
#include "widgets/PrivacyWebEngineView.h"

// Qt framework headers.
#include <QtWebEngineCore>

class UrlRequestInterceptor : public QWebEngineUrlRequestInterceptor
{
    // Include the Q_OBJECT macro.
    Q_OBJECT

public:
    // The default constructor.
    UrlRequestInterceptor(PrivacyWebEngineView *privacyWebEngineViewPointer);

    // The public functions.
    void interceptRequest(QWebEngineUrlRequestInfo &urlRequestInfo) override;

Q_SIGNALS:
    // The signals.
    void applyDomainSettings(const QUrl &url, const bool navigatingHistory) const;
    void displayHttpPingDialog(const QString &httpPingUrl) const;
    void newMainFrameResource() const;
    void requestProcessed(RequestStruct *requestStruct) const;

private:
    // The private variables.
    PrivacyWebEngineView *privacyWebEngineViewPointer;
};
#endif
