/* SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022-2024 Soren Stoutner <soren@stoutner.com>
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef BROWSER_WINDOW_H
#define BROWSER_WINDOW_H

// Application headers.
#include "widgets/TabWidget.h"
#include "widgets/UrlLineEdit.h"

// KDE Frameworks headers.
#include <KConfigDialog>
#include <KToggleFullScreenAction>
#include <KToolBar>
#include <KXmlGuiWindow>

// Qt toolkit headers.
#include <QComboBox>
#include <QLabel>
#include <QProgressBar>

class BrowserWindow : public KXmlGuiWindow
{
    // Include the Q_OBJECT macro.
    Q_OBJECT

public:
    // The default constructor.
    BrowserWindow(bool firstWindow=true, QString *initialUrlStringPointer = nullptr);

    // The public functions.
    QSize sizeHint() const override;

    // The public variables.
    TabWidget *tabWidgetPointer;

public Q_SLOTS:
    // The public slots.
    void applyDomainSettingsInThisWindow();  // This is public so that the domain settings can be reapplied over D-Bus when changed by another instance of Privacy Browser.
    void populateBookmarksInThisWindow();  // This is public so that the bookmarks can be repopulated over D-Bus when changed by another instance of Privacy Browser.

private Q_SLOTS:
    // The private slots.
    void addOrEditDomainSettings();
    void applyDomainSettingsInAllWindows() const;
    void back() const;
    void clearUrlLineEditFocus() const;
    void decrementZoom();
    void editBookmarks();
    void escape() const;
    void findNext() const;
    void findPrevious() const;
    void forward() const;
    void fullScreenRequested(const bool toggleOn);
    void getZoomFactorFromUser();
    void hideFindTextActions() const;
    void hideProgressBar() const;
    void home() const;
    void incrementZoom();
    void loadUrlFromLineEdit(const QString &url) const;
    void newWindow() const;
    void openWithChromium() const;
    void openWithFirefox() const;
    void populateBookmarksInAllWindows() const;
    void processUrlTextChanges(const QString &newUrlText) const;
    void refresh() const;
    void reloadAndBypassCache() const;
    void restoreUrlText() const;
    void runMethodInAllWindows(const QString &methodString) const;
    void showBookmarkContextMenu(const QPoint &point);
    void showCookiesDialog();
    void showDomainSettingsDialog();
    void showFilterListsDialog();
    void showFindTextActions() const;
    void showProgressBar(const int &progress) const;
    void showRequestsDialog();
    void showSettingsDialog();
    void toggleBookmark();
    void toggleDeveloperTools() const;
    void toggleDomStorage() const;
    void toggleFindCaseSensitive() const;
    void toggleJavaScript() const;
    void toggleLocalStorage() const;
    void toggleFullScreen();
    void toggleViewBookmarksToolBar();
    void toggleViewSource() const;
    void toggleViewSourceInNewTab() const;
    void updateCookiesAction(const int numberOfCookies) const;
    void updateDefaultZoomFactor(const double newDefaultZoomFactorDouble);
    void updateDomStorageAction(const bool &isEnabled) const;
    void updateDomainSettingsIndicator(const bool status);
    void updateFindText(const QString &text, const bool findCaseSensitive) const;
    void updateFindTextResults(const QWebEngineFindTextResult &findTextResult) const;
    void updateJavaScriptAction(const bool &isEnabled);
    void updateLocalStorageAction(const bool &isEnabled);
    void updateRequestsAction(const QVector<int> blockedRequestsVector) const;
    void updateSearchEngineActions(const QString &searchEngine, const bool &updateCustomSearchEngineStatus);
    void updateUserAgentActions(const QString &userAgent, const bool &updateCustomUserAgentStatus);
    void updateUrlLineEdit(const QUrl &newUrl);
    void updateViewBookmarksToolBarCheckbox(const bool visible);
    void updateWindowTitle(const QString &title);
    void updateZoomActions(const double zoomFactorDouble);
    void zoomDefault();

private:
    // The private variables.
    KActionCollection *actionCollectionPointer;
    QAction *blockedRequestsActionPointer;
    QAction *bookmarkedActionPointer;
    QList<QPair<QMenu *, QAction *> *> bookmarkFolderFinalActionList;
    QList<QPair<QMenu *, QAction *> *> bookmarksMenuActionList;
    QMenu *bookmarksMenuPointer;
    QList<QPair<QMenu *, QMenu *> *> bookmarksMenuSubmenuList;
    QList<QAction*> bookmarksToolBarActionList;
    QList<QPair<QMenu *, const double> *> bookmarksToolBarMenuList;
    KToolBar *bookmarksToolBarPointer;
    QList<QPair<QMenu *, QAction *> *> bookmarksToolBarSubfolderActionList;
    bool bookmarksToolBarIsVisible = false;
    bool bookmarksToolBarUninitialized = true;
    QAction *cookiesActionPointer;
    QUrl currentUrl;
    QPushButton *currentZoomButtonPointer;
    double currentZoomFactorDouble;
    bool customSearchEngineEnabled;
    bool customUserAgentEnabled;
    double defaultZoomFactorDouble;
    QAction *developerToolsActionPointer;
    QAction *domStorageActionPointer;
    bool domainSettingsApplied = false;
    QAction *easyPrivacyActionPointer;
    QAction *easyListActionPointer;
    QAction *fanboysAnnoyanceListPointer;
    QAction *findCaseSensitiveActionPointer;
    QAction *findNextActionPointer;
    QAction *findPreviousActionPointer;
    QAction *findTextLabelActionPointer;
    QLabel *findTextLabelPointer;
    QAction *findTextLineEditActionPointer;
    KLineEdit *findTextLineEditPointer;
    KToggleFullScreenAction *fullScreenActionPointer;
    QAction *hideFindTextActionPointer;
    QAction *javaScriptActionPointer;
    bool javaScriptEnabled;
    QAction *localStorageActionPointer;
    bool localStorageEnabled;
    KToolBar *navigationToolBarPointer;
    QPalette negativeBackgroundPalette;
    QPalette normalBackgroundPalette;
    QPalette positiveBackgroundPalette;
    QProgressBar *progressBarPointer;
    QAction *refreshActionPointer;
    QAction *requestsActionPointer;
    QAction *restoreUrlTextActionPointer;
    QAction *searchEngineMenuActionPointer;
    QAction *searchEngineMojeekActionPointer;
    QAction *searchEngineMonoclesActionPointer;
    QAction *searchEngineMetagerActionPointer;
    QAction *searchEngineGoogleActionPointer;
    QAction *searchEngineBingActionPointer;
    QAction *searchEngineYahooActionPointer;
    QAction *searchEngineCustomActionPointer;
    QAction *stopActionPointer;
    QAction *ultraPrivacyActionPointer;
    QAction *ultraListActionPointer;
    QAction *userAgentMenuActionPointer;
    QAction *userAgentPrivacyBrowserActionPointer;
    QAction *userAgentWebEngineDefaultActionPointer;
    QAction *userAgentFirefoxLinuxActionPointer;
    QAction *userAgentChromiumLinuxActionPointer;
    QAction *userAgentFirefoxWindowsActionPointer;
    QAction *userAgentChromeWindowsActionPointer;
    QAction *userAgentEdgeWindowsActionPointer;
    QAction *userAgentSafariMacosActionPointer;
    QAction *userAgentCustomActionPointer;
    UrlLineEdit *urlLineEditPointer;
    KToolBar *urlToolBarPointer;
    QAction *viewBookmarksToolBarActionPointer;
    QAction *viewSourceActionPointer;
    QAction *viewSourceInNewTabActionPointer;
    QAction *zoomDefaultActionPointer;
    QAction *zoomFactorActionPointer;
    QAction *zoomInActionPointer;
    QPushButton *zoomMinusButtonPointer;
    QAction *zoomOutActionPointer;
    QPushButton *zoomPlusButtonPointer;

    // The private functions.
    void addBookmarkFolderFinalActions(QMenu *menuPointer, const double folderId, const bool addToList);
    int calculateSettingsInt(const bool settingCurrentlyEnabled, const bool settingEnabledByDefault) const;
    void populateBookmarksMenuSubfolders(const double folderId, QMenu *menuPointer);
    void populateBookmarksToolBar();
    void populateBookmarksToolBarSubfolders(const double folderId, QMenu *menuPointer);
    void updateBookmarkedAction() const;
};
#endif
