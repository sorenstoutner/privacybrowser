/*
 * Copyright 2022, 2024 Soren Stoutner <soren@stoutner.com>.
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * Privacy Browser PC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Privacy Browser PC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Privacy Browser PC.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VIEWONLYDELEGATE_H
#define VIEWONLYDELEGATE_H

// Qt toolkit headers.
#include <QStyledItemDelegate>

class ViewOnlyDelegate : public QStyledItemDelegate
{
    // Include the Q_OBJECT macro.
    Q_OBJECT

public:
    // The default constructor.
    explicit ViewOnlyDelegate(QObject *parentObjectPointer = nullptr);

    // The public functions.
    QWidget* createEditor(QWidget *parentWidgetPointer, const QStyleOptionViewItem &styleOptionsViewItem, const QModelIndex &modelIndex) const override;
};
#endif
