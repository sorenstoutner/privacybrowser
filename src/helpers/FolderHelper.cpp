/*
 * Copyright 2023-2024 Soren Stoutner <soren@stoutner.com>.
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc>.
 *
 * Privacy Browser PC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Privacy Browser PC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Privacy Browser PC.  If not, see <http://www.gnu.org/licenses/>.
 */

// Application headers.
#include "FolderHelper.h"
#include "databases/BookmarksDatabase.h"
#include "structs/BookmarkStruct.h"

// Construct the class.
FolderHelper::FolderHelper() {}

void FolderHelper::populateSubfolders(QTreeWidgetItem *treeWidgetItemPointer, const double initialParentFolderId)
{
    // Get the list of subfolders.
    QList<BookmarkStruct> *subfoldersList = BookmarksDatabase::getSubfolders(treeWidgetItemPointer->text(FOLDER_ID_COLUMN).toDouble());

    // Populate each subfolder.
    for (BookmarkStruct bookmarkStruct : *subfoldersList)
    {
        // Create a tree widget item.
        QTreeWidgetItem *subfolderWidgetItemPointer = new QTreeWidgetItem();

        // Populate the tree widget item.
        subfolderWidgetItemPointer->setText(FOLDER_NAME_COLUMN, bookmarkStruct.name);
        subfolderWidgetItemPointer->setIcon(FOLDER_NAME_COLUMN, bookmarkStruct.favoriteIcon);
        subfolderWidgetItemPointer->setText(FOLDER_ID_COLUMN, QString::number(bookmarkStruct.folderId, 'f', 0));  // Format the folder ID as a floating point with no trailing zeros.

        // Add the subfolder to the tree widget item.
        treeWidgetItemPointer->addChild(subfolderWidgetItemPointer);

        // Select the folder if it is the initial parent folder.
        if (bookmarkStruct.folderId == initialParentFolderId)
            subfolderWidgetItemPointer->setSelected(true);

        // Add any subfolders.
        populateSubfolders(subfolderWidgetItemPointer, initialParentFolderId);
    }
}

void FolderHelper::populateSubfoldersExcept(const double exceptSubfolderDatabaseId, QTreeWidgetItem *treeWidgetItemPointer, const double initialParentFolderId)
{
    // Get the list of subfolders.
    QList<BookmarkStruct> *subfoldersList = BookmarksDatabase::getSubfolders(treeWidgetItemPointer->text(FOLDER_ID_COLUMN).toDouble());

    // Populate each subfolder.
    for (BookmarkStruct bookmarkStruct : *subfoldersList)
    {
        // Determine if this is an excepted folder.
        bool exceptedFolder = bookmarkStruct.databaseId == exceptSubfolderDatabaseId;

        // Create a tree widget item.
        QTreeWidgetItem *subfolderWidgetItemPointer = new QTreeWidgetItem();

        // Populate the tree widget item.
        subfolderWidgetItemPointer->setText(FOLDER_NAME_COLUMN, bookmarkStruct.name);
        subfolderWidgetItemPointer->setIcon(FOLDER_NAME_COLUMN, bookmarkStruct.favoriteIcon);
        subfolderWidgetItemPointer->setText(FOLDER_ID_COLUMN, QString::number(bookmarkStruct.folderId, 'f', 0));  // Format the folder ID as a floating point with no trailing zeros.

        // Disable the folder widget if it is excepted.  All subfolders will automatically be disabled.
        if (exceptedFolder)
            subfolderWidgetItemPointer->setDisabled(true);

        // Add the subfolder to the tree widget item.
        treeWidgetItemPointer->addChild(subfolderWidgetItemPointer);

        // Select the folder if it is the initial parent folder.
        if (bookmarkStruct.folderId == initialParentFolderId)
            subfolderWidgetItemPointer->setSelected(true);

        // Add any subfolders.
        populateSubfoldersExcept(exceptSubfolderDatabaseId, subfolderWidgetItemPointer, initialParentFolderId);
    }
}
