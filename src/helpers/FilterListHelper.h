/* SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2024 Soren Stoutner <soren@stoutner.com>
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FILTER_LIST_HELPER_H
#define FILTER_LIST_HELPER_H

// Application headers.
#include "structs/FilterListStruct.h"
#include "structs/RequestStruct.h"
#include "widgets/PrivacyWebEngineView.h"

// Qt framework headers.
#include <QString>
#include <QWebEngineUrlRequestInfo>

class FilterListHelper
{
public:
    // The default constructor.
    FilterListHelper();

    // The public static disposition constant integers.
    static const int DEFAULT = 0;
    static const int ALLOWED = 1;
    static const int BLOCKED = 2;

    // The public static sublist constant integers.
    static const int MAIN_ALLOWLIST = 0;
    static const int INITIAL_DOMAIN_ALLOWLIST = 1;
    static const int REGULAR_EXPRESSION_ALLOWLIST = 2;
    static const int MAIN_BLOCKLIST = 3;
    static const int INITIAL_DOMAIN_BLOCKLIST = 4;
    static const int REGULAR_EXPRESSION_BLOCKLIST = 5;

    // The public variables.
    FilterListStruct *easyListStructPointer;
    FilterListStruct *easyPrivacyStructPointer;
    FilterListStruct *fanboyAnnoyanceStructPointer;
    FilterListStruct *ultraListStructPointer;
    FilterListStruct *ultraPrivacyStructPointer;

    // The public functions.
    bool checkFilterLists(PrivacyWebEngineView *privacyWebEngineViewPointer, QWebEngineUrlRequestInfo &urlRequestInfo, RequestStruct *requestStructPointer) const;
    QString getDispositionString(int dispositionInt) const;
    QString getNavigationTypeString(int navigationTypeInt) const;
    QString getRequestOptionDispositionString(const FilterOptionDisposition filterOptionDisposition) const;
    QString getResourceTypeString(int resourceTypeInt) const;
    QString getSublistName(int sublistInt) const;

private:
    // The private translated disposition strings.
    QString DEFAULT_STRING;
    QString ALLOWED_STRING;
    QString BLOCKED_STRING;

    // The private translated filter option dispositions.
    QString FILTER_OPTION_NULL;
    QString FILTER_OPTION_APPLY;
    QString FILTER_OPTION_OVERRIDE;

    // The private translated navigation type strings.
    QString NAVIGATION_TYPE_LINK;
    QString NAVIGATION_TYPE_TYPED;
    QString NAVIGATION_TYPE_FORM_SUBMITTED;
    QString NAVIGATION_TYPE_BACK_FORWARD;
    QString NAVIGATION_TYPE_RELOAD;
    QString NAVIGATION_TYPE_REDIRECT;
    QString NAVIGATION_TYPE_OTHER;

    // The private translated resource type strings.
    QString RESOURCE_TYPE_MAIN_FRAME;
    QString RESOURCE_TYPE_SUB_FRAME;
    QString RESOURCE_TYPE_STYLESHEET;
    QString RESOURCE_TYPE_SCRIPT;
    QString RESOURCE_TYPE_IMAGE;
    QString RESOURCE_TYPE_FONT_RESOURCE;
    QString RESOURCE_TYPE_SUB_RESOURCE;
    QString RESOURCE_TYPE_OBJECT;
    QString RESOURCE_TYPE_MEDIA;
    QString RESOURCE_TYPE_WORKER;
    QString RESOURCE_TYPE_SHARED_WORKER;
    QString RESOURCE_TYPE_PREFETCH;
    QString RESOURCE_TYPE_FAVICON;
    QString RESOURCE_TYPE_XHR;
    QString RESOURCE_TYPE_PING;
    QString RESOURCE_TYPE_SERVICE_WORKER;
    QString RESOURCE_TYPE_CSP_REPORT;
    QString RESOURCE_TYPE_PLUGIN_RESOURCE;
    QString RESOURCE_TYPE_NAVIGATION_PRELOAD_MAIN_FRAME;
    QString RESOURCE_TYPE_NAVIGATION_PRELOAD_SUB_FRAME;
    QString RESOURCE_TYPE_UNKNOWN;

    // The private translated sublist strings.
    QString MAIN_ALLOWLIST_STRING;
    QString INITIAL_DOMAIN_ALLOWLIST_STRING;
    QString REGULAR_EXPRESSION_ALLOWLIST_STRING;
    QString MAIN_BLOCKLIST_STRING;
    QString INITIAL_DOMAIN_BLOCKLIST_STRING;
    QString REGULAR_EXPRESSION_BLOCKLIST_STRING;

    // The private functions.
    bool checkAppliedEntry(QWebEngineUrlRequestInfo &urlRequestInfo, RequestStruct *requestStructPointer, const QString &filterListTitle, const int sublistInt, QString urlString,
                           const RequestUrlType requestUrlType, EntryStruct *entryStructPointer) const;
    bool checkDomain(QWebEngineUrlRequestInfo &urlRequestInfo, RequestStruct *requestStructPointer, const QString &filterListTitle, const int sublistInt,
                     EntryStruct *entryStructPointer) const;
    bool checkIndividualList(QWebEngineUrlRequestInfo &urlRequestInfo, RequestStruct *requestStructPointer, FilterListStruct *filterListStructPointer) const;
    bool checkRegularExpression(QWebEngineUrlRequestInfo &urlRequestInfo, RequestStruct *requestStructPointer, const QString &filterListTitle, const int sublistInt,
                                EntryStruct *entryStructPointer) const;
    bool checkRequestOptions(QWebEngineUrlRequestInfo &urlRequestInfo, RequestStruct *requestStructPointer, const QString &filterListTitle, const int sublistInt,
                             EntryStruct *entryStructPointer) const;
    bool checkThirdParty(QWebEngineUrlRequestInfo &urlRequestInfo, RequestStruct *requestStructPointer, const QString &filterListTitle, const int sublistInt, EntryStruct *entryStructPointer) const;
    FilterListStruct* populateFilterList(const QString &filterListFileName) const;
    void prepareFilterListString(QString &filterListString, EntryStruct *entryStructPointer) const;
    bool processRequest(QWebEngineUrlRequestInfo &urlRequestInfo, RequestStruct *requestStructPointer, const QString &filterListTitle, const int sublistInt, EntryStruct *entryStructPointer) const;
};
#endif
