/*
 * Copyright 2024 Soren Stoutner <soren@stoutner.com>.
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * Privacy Browser PC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Privacy Browser PC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Privacy Browser PC.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OVERRIDESTRUCT_H
#define OVERRIDESTRUCT_H

struct OverrideStruct
{
    bool hasOverride = false;
    bool font = false;
    bool image = false;
    bool mainFrame = false;
    bool media = false;
    bool object = false;
    bool other = false;
    bool ping = false;
    bool script = false;
    bool styleSheet = false;
    bool subFrame = false;
    bool xmlHttpRequest = false;
};
#endif
