/*
 * Copyright 2024 Soren Stoutner <soren@stoutner.com>.
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * Privacy Browser PC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Privacy Browser PC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Privacy Browser PC.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FILTER_LIST_STRUCT_H
#define FILTER_LIST_STRUCT_H

// Application headers.
#include "EntryStruct.h"

// C++ headers.
#include <forward_list>

// Qt toolkit headers.
#include <QString>

struct FilterListStruct
{
    // The strings.
    QString title;
    QString version;
    QString filePath;

    // The filter lists.
    std::forward_list<EntryStruct *> *mainAllowListPointer = new std::forward_list<EntryStruct *>;
    std::forward_list<EntryStruct *> *initialDomainAllowListPointer = new std::forward_list<EntryStruct *>;
    std::forward_list<EntryStruct *> *regularExpressionAllowListPointer = new std::forward_list<EntryStruct *>;
    std::forward_list<EntryStruct *> *mainBlockListPointer = new std::forward_list<EntryStruct *>;
    std::forward_list<EntryStruct *> *initialDomainBlockListPointer = new std::forward_list<EntryStruct *>;
    std::forward_list<EntryStruct *> *regularExpressionBlockListPointer = new std::forward_list<EntryStruct *>;
};
#endif
