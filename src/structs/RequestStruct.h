/*
 * Copyright 2024 Soren Stoutner <soren@stoutner.com>.
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * Privacy Browser PC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Privacy Browser PC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Privacy Browser PC.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef REQUEST_STRUCT_H
#define REQUEST_STRUCT_H

// Application headers.
#include "EntryStruct.h"

enum RequestUrlType
{
    Url = 0,
    TruncatedUrl = 1,
    UrlWithSeparators = 2,
    TruncatedUrlWithSeparators = 3
};

struct RequestStruct
{
    int dispositionInt = 0;  // `0` is the default disposition.
    EntryStruct entryStruct;
    QString filterListTitle;
    QString firstPartyHost;
    bool isThirdPartyRequest;
    RequestUrlType matchedUrlType;
    int navigationTypeInt;
    QString requestMethodString;
    int resourceTypeInt;
    int sublistInt = -1;  // The sublist should be initialized because it is not always set later.
    QString truncatedUrlString;
    QString truncatedUrlStringWithSeparators;
    QString urlString;
    QString urlStringWithSeparators;
    QString webPageUrlString;
};
#endif
