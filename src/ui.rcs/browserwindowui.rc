<?xml version="1.0" encoding="UTF-8"?>

<!--
  Copyright 2022-2024 Soren Stoutner <soren@stoutner.com>.

  This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.

  Privacy Browser PC is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Privacy Browser PC is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Privacy Browser PC.  If not, see <http://www.gnu.org/licenses/>. -->

<!-- Documentation at <https://techbase.kde.org/Development/Architecture/KDE4/XMLGUI_Technology>. -->
<!-- Better documentation at <https://invent.kde.org/frameworks/kxmlgui/-/blob/master/src/kxmlgui.xsd>. -->
<!-- This file interacts with the default template located at /etc/xdg/ui/ui_standards.rc, which is part of the libkf5xmlgui-data package. -->
<gui
    name="privacybrowser"
    version="1"
    xmlns="http://www.kde.org/standards/kxmlgui/1.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.kde.org/standards/kxmlgui/1.0 http://www.kde.org/standards/kxmlgui/1.0/kxmlgui.xsd" >

    <!-- The menu bar. -->
    <MenuBar>
        <!-- File. -->
        <Menu name="file">
            <Action name="new_tab" append="new_merge" />
            <Action name="new_window" append="new_merge" />

            <Action name="save_archive" append="save_merge" />

            <Action name="open_with_firefox" />
            <Action name="open_with_chromium" />
        </Menu>

        <!-- View. -->
        <Menu name="view">
            <Action name="zoom_default" append="view_zoom_merge" />

            <Action name="reload_and_bypass_cache" />
            <Action name="stop" />

            <Separator />

            <Action name="view_source" />
            <Action name="view_source_in_new_tab" />
            <Action name="developer_tools" />
        </Menu>

        <!-- On-the-fly Settings. -->
        <Menu name="on_the_fly_settings"> <text>On-The-Fly Settings</text>
            <Action name="javascript" />
            <Action name="local_storage" />
            <Action name="dom_storage" />

            <Menu name="user_agent">
                <Action name="user_agent_privacy_browser" />
                <Action name="user_agent_webengine_default" />
                <Action name="user_agent_firefox_linux" />
                <Action name="user_agent_chromium_linux" />
                <Action name="user_agent_firefox_windows" />
                <Action name="user_agent_chrome_windows" />
                <Action name="user_agent_edge_windows" />
                <Action name="user_agent_safari_macos" />
                <Action name="user_agent_custom" />
            </Menu>

            <Action name="zoom_factor" />

            <Separator />

            <Menu name="search_engine">
                <Action name="search_engine_mojeek" />
                <Action name="search_engine_monocles" />
                <Action name="search_engine_metager" />
                <Action name="search_engine_google" />
                <Action name="search_engine_bing" />
                <Action name="search_engine_yahoo" />
                <Action name="search_engine_custom" />
            </Menu>
        </Menu>

        <!-- Filter lists. -->
        <Menu name="filter_lists"> <text>Filter Lists</text>
            <Action name="ultraprivacy" />
            <Action name="ultralist" />
            <Action name="easyprivacy" />
            <Action name="easylist" />
            <Action name="fanboys_annoyance_list" />

            <Separator />

            <Action name="requests" />
            <Action name="view_filter_lists" />
        </Menu>

        <!-- Bookmarks. -->
        <Menu name="bookmarks">
            <Action name="view_bookmarks_toolbar" />

            <Separator />
        </Menu>

        <!-- Settings. -->
        <Menu name="settings">
            <Action name="domain_settings" />
            <Action name="cookies" />
        </Menu>
    </MenuBar>

    <!-- The main toolbar is removed. -->
    <ToolBar name="mainToolBar" deleted="true" />

    <!-- The navigation toolbar. -->
    <ToolBar name="navigation_toolbar" iconText="icononly"> <text>Navigation Toolbar</text>
        <Action name="go_back" />
        <Action name="go_forward" />
        <Action name="view_redisplay" />
        <Action name="stop" />
        <Action name="go_home" />
    </ToolBar>

    <!-- The URL toolbar. -->
    <ToolBar name="url_toolbar" iconText="icononly"> <text>URL Toolbar</text>
        <Action name="blocked_requests" />
        <Action name="javascript" />
        <Action name="local_storage" />
        <Action name="dom_storage" />
        <Action name="edit_find_next" />
        <Action name="edit_find_prev" />
        <Action name="find_case_sensitive" />
        <Action name="hide_find_actions" />
    </ToolBar>

    <!-- The bookmarks toolbar. The newline aspect of this doesn't currently work. -->
    <ToolBar name="bookmarks_toolbar" iconText="icontextright" newline="true" hidden="true"> <text>Bookmarks Toolbar</text> </ToolBar>
</gui>
