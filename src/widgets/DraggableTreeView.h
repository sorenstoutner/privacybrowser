/* SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2023-2024 Soren Stoutner <soren@stoutner.com>
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DRAGGABLETREEVIEW_H
#define DRAGGABLETREEVIEW_H

// Qt toolkit headers.
#include <QTreeView>

class DraggableTreeView : public QTreeView
{
    // Include the Q_OBJECT macro.
    Q_OBJECT

public:
    // The default contructor.
    explicit DraggableTreeView(QWidget *parentWidget = nullptr);

Q_SIGNALS:
    // The signals.
    void bookmarksMoved() const;

protected:
    void dropEvent(QDropEvent *event) override;

private:
    // The private functions
    QList<int>* getRootSelectedDatabaseIds(QList<int> *selectedDatabaseIdsPointer) const;
    void moveToBeginningOfFolder(const QModelIndex &dropModelIndex, QList<int> *selectedDatabaseIdsListPointer, QList<int> *rootSelectedDatabaseIdsListPointer,
                                 std::list<double> *previousParentFolderIdListPointer) const;
};
#endif
