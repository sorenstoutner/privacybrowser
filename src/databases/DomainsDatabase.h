/* SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022-2024 Soren Stoutner <soren@stoutner.com>
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DOMAINS_DATABASE_H
#define DOMAINS_DATABASE_H

// Qt framework headers.
#include <QtSql>

class DomainsDatabase
{
public:
    // The default constructor.
    DomainsDatabase();

    // The public functions.
    static void addDatabase();
    static void addDomain(const QString &domainName);
    static void addDomain(const QString &domainName, const int javaScriptInt, const int localStorageInt, const int domStorageInt, const QString &userAgentDatabaseString, const int ultraPrivacyInt,
                          const int ultraListInt, const int easyPrivacyInt, const int easyListInt, const int fanboysAnnoyanceListInt, const int zoomFactorInt, const double currentZoomFactorDouble);
    static QSqlQuery getDomainQuery(const QString &hostname);

    // The public int constants.
    static const int SYSTEM_DEFAULT = 0;
    static const int ENABLED = 1;
    static const int DISABLED = 2;
    static const int CUSTOM = 1;

    // The public constants.
    static const QString CONNECTION_NAME;
    static const QString CUSTOM_ZOOM_FACTOR;
    static const QString DOM_STORAGE;
    static const QString DOMAIN_NAME;
    static const QString DOMAINS_TABLE;
    static const QString EASYLIST;
    static const QString EASYPRIVACY;
    static const QString FANBOYS_ANNOYANCE_LIST;
    static const QString ID;
    static const QString JAVASCRIPT;
    static const QString LOCAL_STORAGE;
    static const QString ULTRALIST;
    static const QString ULTRAPRIVACY;
    static const QString USER_AGENT;
    static const QString ZOOM_FACTOR;

private:
    // The private static constants.
    static const int SCHEMA_VERSION;
};
#endif
