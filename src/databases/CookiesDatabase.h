/* SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022-2023 Soren Stoutner <soren@stoutner.com>
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef COOKIESDATABASE_H
#define COOKIESDATABASE_H

// Qt framework headers.
#include <QNetworkCookie>
#include <QtSql>

class CookiesDatabase
{
public:
    // The default constructor.
    CookiesDatabase();

    // The public functions.
    static void addDatabase();
    static void addCookie(const QNetworkCookie &cookie);
    static int cookieCount();
    static void deleteAllCookies();
    static void deleteCookie(const QNetworkCookie &cookie);
    static QList<QNetworkCookie*>* getCookies();
    static QNetworkCookie* getCookieById(const int &id);
    static bool isDurable(const QNetworkCookie &cookie);
    static bool isUpdate(const QNetworkCookie &cookie);
    static void updateCookie(const QNetworkCookie &cookie);
    static void updateCookie(const QNetworkCookie &oldCookie, const QNetworkCookie &newCookie);

    // The public constants.
    static const QString CONNECTION_NAME;
    static const QString COOKIES_TABLE;
    static const QString DOMAIN;
    static const QString EXPIRATION_DATE;
    static const QString HTTP_ONLY;
    static const QString ID;
    static const QString NAME;
    static const QString PATH;
    static const QString SECURE;
    static const QString VALUE;

private:
    // The private static constants.
    static const int SCHEMA_VERSION;
};
#endif
