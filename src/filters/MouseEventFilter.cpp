/* SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022, 2024 Soren Stoutner <soren@stoutner.com>
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// Application headers.
#include "MouseEventFilter.h"

// Qt headers.
#include <QEvent>
#include <QMouseEvent>

// The primary constructor.
MouseEventFilter::MouseEventFilter() : QObject() {};

// Construct the class.
bool MouseEventFilter::eventFilter(QObject *objectPointer, QEvent *eventPointer)
{
    // Only process mouse button press events.
    if (eventPointer->type() == QEvent::MouseButtonPress)
    {
        // Tell the compiler to ignore the unused object pointer.
        (void) objectPointer;

        // Cast the event to a mouse event.
        QMouseEvent *mouseEventPointer = static_cast<QMouseEvent *>(eventPointer);

        // Run the command according to the button that was pushed.
        switch (mouseEventPointer->button())
        {
            case (Qt::BackButton):
                // Tell the WebEngine to go back.
                Q_EMIT mouseBack();

                // Consume the event.
                return true;

            case (Qt::ForwardButton):
                // Tell the WebEngine to go forward.
                Q_EMIT mouseForward();

                // Consume the event.
                return true;

            default:
                // Do not consume the event.
                return false;
        }
    }

    // Do not consume the event.
    return false;
}
